﻿Last Update : 2017/09/14
Prototype stage.

Important files :
- Initializer.cs
- PracticeCycleManager.cs
- StandardTableExerciceSheet.cs

Initializer should run befopre all other script to make sure all data are ready to be used.
It is set in the script order execution panel to run before all scripts.

PracticeCycleManager take care of all user input and update during exercises.

StandardTableExerciceSheet is themain entry point in order to set all data to create table and keys.

