﻿using UnityEngine;
using UnityEngine.UI;

namespace VPR.Updatable
{
	public class TextBlink : MonoBehaviour
	{
		#region fields
		private Text text;
		private Color textColor;
		[SerializeField]
		private int index;
		private float elapsedTime;
		private bool isVisible;
		#endregion

		#region constant
		public const float blinkTime = 0.3f;
		#endregion

		#region methods
		void Awake()
		{
			text = GetComponent<Text>();
			textColor = text.color;
			isVisible = true;
			enabled = false;
		}

		public void SetIndex(int index)
		{
			this.index = index;
		}

		public void StartBlink()
		{
			CheckIfTextExist();

			if(string.IsNullOrEmpty(text.text)) return;
			enabled = true;
		}

		public void DismissSymbol()
		{
			text.enabled = false;
			enabled = false;
		}

		public void DismissSymbol(int index)
		{
			if(this.index != index) return;
			text.enabled = false;
			enabled = false;
		}

		void Update ()
		{
			if(text != null)
			{
				elapsedTime += Time.deltaTime;

				if(elapsedTime > blinkTime)
				{
					isVisible = !isVisible;
					elapsedTime = 0f;
				}

				SetTextColor();
			}
		}

		private void SetTextColor()
		{
			textColor.a = isVisible ? 1 : 0;
			text.color = textColor;
		}

		public void ResetSymbols()
		{
			CheckIfTextExist();

			text.enabled = true;
			textColor.a = 1;
			text.color = textColor;
			isVisible = true;
			elapsedTime = 0f;
		}

		private void CheckIfTextExist()
		{
			if(text == null)
			{
				text = GetComponent<Text>();
			}
		}
		#endregion
	}
}

