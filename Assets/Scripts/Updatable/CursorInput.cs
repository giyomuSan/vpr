﻿using UnityEngine;
using UnityEngine.UI;

namespace VPR.Updatable
{
	/// <summary>
	/// Not Used
	/// </summary>
	public class CursorInput : MonoBehaviour 
	{
		#region fields
		public SentenceType Sentence;
		public float BlinkTime;

		private Text cursorText;
		private Color textColor;
		private string currentPhrase;
		private float elapsedTime;
		private bool isVisible;
		#endregion

		#region constant
		private const string cursor = "|";
		private const string space = "";
		private const string openColorTag = "<color=#32323200>";
		private const string closeColorTag = "</color>";
		#endregion

		#region methods
		private void Start()
		{
			currentPhrase = string.Empty;
			cursorText = GetComponent<Text>();
			textColor = cursorText.color;
			SetTextColor();
		}

		private void Update()
		{
			if(cursorText != null && CanShowCursor())
			{
				elapsedTime += Time.deltaTime;

				if(elapsedTime > BlinkTime)
				{
					isVisible = !isVisible;
					elapsedTime = 0f;
				}

				SetTextColor();
				cursorText.text = currentPhrase + space + cursor;
			}
		}

		private void SetTextColor()
		{
			textColor.a = isVisible ? 1 : 0;
			cursorText.color = textColor;
		}

		private bool CanShowCursor()
		{
			var currentSentence = PracticeCycleManager.Instance.CurrentSentence;

			// each sentence first word, symbol still visible , no cursor should be showing up
			if(string.IsNullOrEmpty(currentPhrase)) return false;

			if(Sentence != PracticeCycleManager.Instance.CurrentSentence)
			{
				isVisible = false;
				SetTextColor();
				return false;
			}

			return true;
		}

		public void UpdateCursorPosition(string phrase)
		{
			currentPhrase = openColorTag + phrase + closeColorTag;
		}

		private void OnDisable()
		{
			currentPhrase = string.Empty;
		}
		#endregion
	}
}
