﻿using UnityEngine;
using UnityEngine.UI;

namespace VPR.Updatable
{
	public class KeyHighlight : MonoBehaviour
	{
		#region fields
		public float BlinkTime;
		public Color32 TargetColor;

		private Image image;
		private Color32 initialColor;
		private float elapsedTime;
		private bool isTargetColor;
		#endregion

		#region methods
		private void Start ()
		{
			image = GetComponent<Image>();
			initialColor = image.color;
			enabled = false;
		}

		public void StartHighlight()
		{
			if(enabled) return;

			isTargetColor = true;
			image.color = TargetColor;
			enabled = true;
		}

		public void StopHighlight()
		{
			image.color = initialColor;
			enabled = false;
		}

		private void Update()
		{
			// simple blink test for now.
			elapsedTime += Time.deltaTime;
			if(elapsedTime > BlinkTime)
			{
				isTargetColor = !isTargetColor;

				var color = isTargetColor ? TargetColor : initialColor;
				image.color = color;
				elapsedTime = 0f;
			}
		}
		#endregion
	}
}

