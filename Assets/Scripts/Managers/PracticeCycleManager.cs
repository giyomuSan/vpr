﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace VPR
{
	public class PracticeCycleManager : MonoBehaviourSingleton<PracticeCycleManager>
	{
		#region fields
		[SerializeField]
		private SentenceType currentSentenceType;
		private int sentenceIndex;
		private int symbolIndex;
		private int symbolCountMax;
		[SerializeField]
		private bool isExerciseStarted;
		private TableCellExerciseData exerciceData;
		[SerializeField]
		private int orderIndex = 0;
		private int retry;

		private AudioSource audio;
		[SerializeField]
		private AudioClip rightAnswer;
		[SerializeField]
		private AudioClip wrongAnswer;
		[SerializeField]
		private AudioClip cellCompleted;
		#endregion

		#region properties
		public SentenceType CurrentSentence{get{return currentSentenceType;}}
		public bool IsExerciseStarted{get{return isExerciseStarted;}}
		public int GetInteractableCellIndex
		{
			get{return Initializer.Instance.GetInteractableCellIndex(orderIndex);}
		}
		#endregion

		#region events
		public event Action<SentenceType, string> OnKeyBoardInput;
		public void NotifyKeyboardInput(SentenceType sentenceType, string inputValue)
		{
			if(OnKeyBoardInput != null) OnKeyBoardInput(sentenceType, inputValue);
		}
		#endregion

		#region methods
		private void Awake()
		{
			audio = GetComponent<AudioSource>();	
		}

		public void StartExercise(int cellIndex)
		{
			int index = Initializer.Instance.CurrentSheetIndex;
			exerciceData = Initializer.Instance.GetExerciceData(index, cellIndex);

			string keyName = Initializer.Instance.GetTableKeyName(index);
			Logger.Message(this, "StartExercise", "Selected Index = " + cellIndex + " for table key : " + keyName);

			currentSentenceType = SentenceType.Affirmative;
			sentenceIndex = (int)currentSentenceType;
			symbolIndex = 0;
			symbolCountMax = exerciceData.Parameters[sentenceIndex].Length;

			isExerciseStarted = true;
		}

		public void StopExercise()
		{
			isExerciseStarted = false;

			UIPanelManager.Instance.SelectionPopupPanel.Dismiss();

			int completed = GetInteractableCellIndex;

			orderIndex++;
			int next = GetInteractableCellIndex;

			// check if the table is fully completed
			if(Initializer.Instance.IsLastIndex(orderIndex))
			{
				// TODO : for now loop on the same table
				orderIndex = 0;
				UIPanelManager.Instance.SelectionPanel.ResetCell(orderIndex);
				Initializer.Instance.LoadNextSheet();
			}
			else
			{
				UIPanelManager.Instance.SelectionPanel.SetNextInteractableCell(completed, next);
			}

			// reset to afirmative sentence, in order for text blink to pickup on next popup call
			currentSentenceType = SentenceType.Affirmative;
		}

		public void ResetExercise()
		{
			//if(!isExerciseStarted) return;

			isExerciseStarted = false;

			orderIndex = 0;
			sentenceIndex = 0;
			symbolIndex = 0;
			currentSentenceType = SentenceType.Affirmative;

			int start = GetInteractableCellIndex;

			UIPanelManager.Instance.SelectionPanel.ResetCell(start);
			UIPanelManager.Instance.SelectionPopupPanel.Dismiss();

			// TODO : have to check the flow here since some value stay when the popup dismiss

		}

		public void CheckInput(string inputValue, AudioClip voice)
		{
			if(IsInputValid(inputValue))
			{
				if(retry >= 2)
				{
					HighlightCorrectKey(false);
					retry = 0;
				}

				UIPanelManager.Instance.SelectionPopupPanel.HideSymbol(currentSentenceType, symbolIndex);
				inputValue = FormatInput(inputValue);

				NotifyKeyboardInput(currentSentenceType, inputValue);
				if(voice != null) audio.PlayOneShot(voice);
				symbolIndex++;

				CheckForNextSentence();
			}
			else
			{
				audio.PlayOneShot(wrongAnswer);
				retry++;

				if(retry >= 2)
				{
					HighlightCorrectKey(true);
				}
			}
		}

		private bool IsInputValid(string inputValue)
		{
			LogCurrentExpectedAnswer();

			string answer = exerciceData.Parameters[sentenceIndex][symbolIndex];
			return answer.ToLower() == inputValue.ToLower();
		}

		private void HighlightCorrectKey(bool state)
		{
			
			string expectedInput = exerciceData.Parameters[sentenceIndex][symbolIndex];

			Debug.Log(expectedInput);

			UIPanelManager.Instance.KeyboardPanel.HighlightKey(expectedInput, state);
		}

		private string FormatInput(string inputValue)
		{
			// Remove suffix index in case of a verb
			char last = inputValue[inputValue.Length - 1];

			if(char.IsNumber(last))
			{
				inputValue = inputValue.Remove(inputValue.Length - 1);
			}
				
			inputValue = inputValue.ToLower();

			// Capitalize the first word in the sentence
			if(symbolIndex == 0)
			{
				return char.ToUpper(inputValue[0]) + inputValue.Substring(1);
			}

			// Add . or ? at the end of the phrase
			if(symbolIndex == symbolCountMax - 1)
			{
				string punc = sentenceIndex == (int)SentenceType.Interrogative ? " ?" : ".";
				inputValue += punc;
			}

			return inputValue;
		}

		private void CheckForNextSentence()
		{
			if(symbolIndex >= symbolCountMax)
			{
				if(sentenceIndex == (int)SentenceType.Negative)
				{
					audio.PlayOneShot(cellCompleted);

					UIPanelManager.Instance.SelectionPopupPanel.GetAnimation.gameObject.SetActive(true);
					UIPanelManager.Instance.SelectionPopupPanel.GetAnimation.PlayAnimation(() =>
					{
						StopExercise();
						//return;	
					});
				}
				else
				{
					sentenceIndex++;
					currentSentenceType = (SentenceType)sentenceIndex;

					symbolIndex = 0;
					symbolCountMax = exerciceData.Parameters[sentenceIndex].Length;

					UIPanelManager.Instance.SelectionPopupPanel.NextLine(currentSentenceType);
				}
			}
		}
		#endregion

		#region debug
		private void LogCurrentExpectedAnswer()
		{
			string answer = exerciceData.Parameters[sentenceIndex][symbolIndex];
			Logger.Message(this, "LogCurrentExpectedAnswer", "Expecting : " + answer);
		}
		#endregion
	}
}

