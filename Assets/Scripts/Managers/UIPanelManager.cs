﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VPR.View;

namespace VPR
{
	public class UIPanelManager : MonoBehaviourSingleton<UIPanelManager>
	{
		#region fields
		public SelectionPanel SelectionPanel;
		public SelectionPopupPanel SelectionPopupPanel;
		public DropdownListPanel DropdownListPanel;
		public KeyboardPanel KeyboardPanel;
		#endregion
	}
}
