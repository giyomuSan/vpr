﻿using UnityEngine;
using System;
using System.Collections.Generic;
using VPR.Core;

namespace VPR
{
	public class Initializer : MonoBehaviourSingleton<Initializer>
	{
		#region fields
		public TableType SelectedTableType;
		[SerializeField]
		private int currentSheetIndex = 0;
		private StandardTableExerciseSheet table;
		#endregion

		#region properties
		public int CurrentSheetIndex{get{return currentSheetIndex;}}
		#endregion

		#region events
		public event Action OnTableChange;
		public event Action OnSheetChange;
		#endregion

		#region methods
		void Awake()
		{
			table = new StandardTableExerciseSheet(SelectedTableType);
		}

		public ExerciseSheet GetSheetForVerb(string verb)
		{
			if(table != null) return table.GetSheetAtIndex(verb);

			return null;
		}
			
		public List<string[]> GetSheetheaderParameters()
		{
			return table.GetSheetHeader();
		}

		public string[] GetCurrentSheetHeader()
		{
			// TODO : index for parameters will need update during going through table
			return GetSheetheaderParameters()[currentSheetIndex];
		}

		public TableCellExerciseData GetExerciceData(int index, int cellIndex)
		{
			string key = GetTableKeyName(index);
			return GetExerciceData(key, cellIndex);
		}

		public string GetTableKeyName(int index)
		{
			return table.GetTableKey(index);
		}

		public TableCellExerciseData GetExerciceData(string verbIndex, int cellIndex)
		{
			return table.GetExerciceData(verbIndex, cellIndex);
		}

		public int GetInteractableCellIndex(int index)
		{
			return table.GetCellIndexFromOrderIndex(index);
		}

		public bool IsLastIndex(int index)
		{
			return table.GetLastOrderIndex() < index;
		}

		public string GetKeyboardTableFilename()
		{
			return table.GetKeyboardFileAtIndex(currentSheetIndex);
		}

		public void LoadTable()
		{
			table = null;
			currentSheetIndex = 0;

			// TODO : quicly put for test
			if(SelectedTableType == TableType.Standard)
			{
				table = new StandardTableExerciseSheet(TableType.ToBe);
				SelectedTableType = TableType.ToBe;
			}
			else if(SelectedTableType == TableType.ToBe)
			{
				table = new StandardTableExerciseSheet(TableType.Standard);
				SelectedTableType = TableType.Standard;
			}

			PracticeCycleManager.Instance.ResetExercise();

			if(OnTableChange != null) OnTableChange();
			if(OnSheetChange != null) OnSheetChange();
		}

		public void LoadPreviousSheet()
		{
			if(currentSheetIndex > 0) currentSheetIndex--;
			PracticeCycleManager.Instance.ResetExercise();

			if(OnSheetChange != null) OnSheetChange();
		}

		public void LoadNextSheet()
		{
			int max = table.GetMaxTableIndex();
			Debug.Log(max);

			// Just a temp hack for std table
			if(SelectedTableType == TableType.Standard) max = 3;

			currentSheetIndex = Mathf.Clamp(++currentSheetIndex,0,max);
			PracticeCycleManager.Instance.ResetExercise();

			if(OnSheetChange != null) OnSheetChange();
		}
		#endregion
	}
}

