using UnityEngine;

public static class Logger
{
	#region fields
	private const int MESSAGE = 0;
	private const int ERROR = 1;

	#if UNITY_PRO_LICENSE
	private const string CLASS_COLOR = "<color=#74c0d9>";
	private const string METHOD_COLOR = "<color=#b5d1da>";
	private const string MESSAGE_COLOR = "<color=#ffb400>";
	private const string ERROR_COLOR = "<color=#fd8181>";
	#else
	private const string CLASS_COLOR = "<color=#132d5a>";
	private const string METHOD_COLOR = "<color=#276198>";
	private const string MESSAGE_COLOR = "<color=#08080f>";
	private const string ERROR_COLOR = "<color=#a90000>";
	#endif

	private const string CLOSING_TAG = "</color>";
	#endregion


	#region methods
	public static void Message(string className, string methodName, string message)
	{
		#if UNITY_EDITOR
		var formatted = Format(className, methodName, message, MESSAGE);
		Debug.Log(formatted);
		#endif
	}

	public static void Message(Object className, string methodName, string message)
	{
		#if UNITY_EDITOR
		var formatted = Format(className, methodName, message, MESSAGE);
		Debug.Log(formatted);
		#endif
	}

	public static void Error(string className, string methodName, string message)
	{
		#if UNITY_EDITOR
		var formatted = Format(className, methodName, message, ERROR);
		Debug.LogError(formatted);
		#endif
	}

	public static void Error(Object className, string methodName, string message)
	{
		#if UNITY_EDITOR
		var formatted = Format(className, methodName, message, ERROR);
		Debug.LogError(formatted);
		#endif
	}
	#endregion

	#region helpers
	private static string Format(string className, string methodName, string message, int logType)
	{
		#if UNITY_EDITOR
		className = CLASS_COLOR + className + CLOSING_TAG;
		methodName = METHOD_COLOR + methodName + CLOSING_TAG;

		switch(logType)
		{
			case MESSAGE:
				message = MESSAGE_COLOR + message + CLOSING_TAG;
			break;
			case ERROR:
				message = ERROR_COLOR + message + CLOSING_TAG;
			break;
		default:
			message = MESSAGE_COLOR + message + CLOSING_TAG;
			break;
		}
		#endif

		return string.Format("{0}.{1}() : {2}", className, methodName, message);
	}

	private static string Format(Object className, string methodName, string message, int logType)
	{
		var classNameToString = className.GetType().ToString();
		return Format(classNameToString, methodName, message, logType);
	}
	#endregion
}

