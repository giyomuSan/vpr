﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VPR.Test
{
	[ExecuteInEditMode]
	public class ReferencePanelEditorSwitch : MonoBehaviour 
	{
		public enum ReferencePanel
		{
			SF301 = 0,
			SF302,
			SF303,
			SF304,
			SF305,
			SF306
		}

		public ReferencePanel SelectedPanel;

		[Range(0,1)]
		public float AlphaValue = 1f;

		[SerializeField]
		private Image[] images;

		#if UNITY_EDITOR
		private void Update()
		{
			Image selectedImage = images[(int)SelectedPanel];

			if(selectedImage != null)
			{
				if(!selectedImage.enabled) selectedImage.enabled = true;
				selectedImage.color = new Color(1, 1, 1, AlphaValue);
			}

			for(int i = 0; i < images.Length; i++)
			{
				if(images[i] == selectedImage) continue;
				images[i].enabled = false;
			}
		}
		#endif
	}
}
