﻿using System.Collections.Generic;

namespace VPR.Core
{
	public class DefaultTable : Table
	{
		public DefaultTable(string tableName) : base(tableName){}

		protected override void CreateTable()
		{
			tableCollection = new Dictionary<int, TableData>()
			{
				// Blank cell ( top left corner of the grid
				{1,new TableData(TableData.CellAttribute.Blank, VerbTime.None, VerbMode.None, null, CellColor.Transparent)},

				// Verb mode header ( top row )
				{2,new TableData(TableData.CellAttribute.VerbModeHeader, VerbTime.None, VerbMode.Simple, null, CellColor.Default)},
				{3,new TableData(TableData.CellAttribute.VerbModeHeader, VerbTime.None, VerbMode.Progressive, null, CellColor.Default)},
				{4,new TableData(TableData.CellAttribute.VerbModeHeader, VerbTime.None, VerbMode.Perfect, null, CellColor.Default)},
				{5,new TableData(TableData.CellAttribute.VerbModeHeader, VerbTime.None, VerbMode.PerfectProgressive, null, CellColor.Default)},

				// Present all mode
				{6,new TableData(TableData.CellAttribute.VerbTimeHeader, VerbTime.Present, VerbMode.None, null, CellColor.Default)},

				{7,new TableData(TableData.CellAttribute.Default, VerbTime.Present, VerbMode.Simple, new TableLineWrapper(CellColor.LightRed, "1 or 2", "1"), CellColor.LightBlue)},
				{8,new TableData(TableData.CellAttribute.Default, VerbTime.Present, VerbMode.Progressive, new TableLineWrapper("5"), CellColor.LightBlue)},
				{9,new TableData(TableData.CellAttribute.Default, VerbTime.Present, VerbMode.Perfect, new TableLineWrapper("4"), CellColor.LightBlue)},
				{10,new TableData(TableData.CellAttribute.Default, VerbTime.Present, VerbMode.PerfectProgressive, new TableLineWrapper("5"), CellColor.Green)},

				// Past all mode
				{11,new TableData(TableData.CellAttribute.VerbTimeHeader, VerbTime.Past, VerbMode.None, null, CellColor.Default)},

				{12,new TableData(TableData.CellAttribute.Default, VerbTime.Past, VerbMode.Simple, new TableLineWrapper(CellColor.LightRed, "3", "1"), CellColor.LightBlue)},
				{13,new TableData(TableData.CellAttribute.Default, VerbTime.Past, VerbMode.Progressive, new TableLineWrapper("5"), CellColor.LightBlue)},
				{14,new TableData(TableData.CellAttribute.Default, VerbTime.Past, VerbMode.Perfect, new TableLineWrapper("4"), CellColor.LightBlue)},
				{15,new TableData(TableData.CellAttribute.Default, VerbTime.Past, VerbMode.PerfectProgressive, new TableLineWrapper("5"), CellColor.Green)},

				// Future all mode
				{16,new TableData(TableData.CellAttribute.VerbTimeHeader, VerbTime.Future, VerbMode.None, null, CellColor.Default)},

				{17,new TableData(TableData.CellAttribute.Default, VerbTime.Future, VerbMode.Simple, new TableLineWrapper("1"), CellColor.LightBlue)},
				{18,new TableData(TableData.CellAttribute.Default, VerbTime.Future, VerbMode.Progressive, new TableLineWrapper("5"), CellColor.Green)},
				{19,new TableData(TableData.CellAttribute.Default, VerbTime.Future, VerbMode.Perfect, new TableLineWrapper("4"), CellColor.Green)},
				{20,new TableData(TableData.CellAttribute.Default, VerbTime.Future, VerbMode.PerfectProgressive, new TableLineWrapper("5"), CellColor.LightBrown)},

				// Modal all mode
				{21,new TableData(TableData.CellAttribute.VerbTimeHeader, VerbTime.Modal, VerbMode.None, null, CellColor.Default)},

				{22,new TableData(TableData.CellAttribute.Default, VerbTime.Modal, VerbMode.Simple, new TableLineWrapper("1"), CellColor.LightBlue)},
				{23,new TableData(TableData.CellAttribute.Default, VerbTime.Modal, VerbMode.Progressive, new TableLineWrapper("5"), CellColor.Green)},
				{24,new TableData(TableData.CellAttribute.Default, VerbTime.Modal, VerbMode.Perfect, new TableLineWrapper("4"), CellColor.Green)},
				{25,new TableData(TableData.CellAttribute.Default, VerbTime.Modal, VerbMode.PerfectProgressive, new TableLineWrapper("5"), CellColor.LightBrown)},
			};
		}
	}
}

