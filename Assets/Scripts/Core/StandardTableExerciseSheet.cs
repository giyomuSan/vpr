﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using VPR.model;

namespace VPR.Core
{
	// Should be renamed TableExerciseSheet;
	public class StandardTableExerciseSheet
	{
		#region class
		private class HeaderData
		{
			private int index;
			private string tableKey;
			private string tableFile;
			private string keyboardFile;

			public int Index{get{return index;}}
			public string TableKey{get{return tableKey;}}
			public string TableFile{get{return tableFile;}}
			public string KeyboardFile{get{return keyboardFile;}}

			public HeaderData(int index, string tableKey, string tableFile, string keyboardFile)
			{
				this.index = index;
				this.tableKey = tableKey;
				this.tableFile = tableFile;
				this.keyboardFile = keyboardFile;
			}
		}
		#endregion

		#region fields
		private List<Dictionary<string,object>> headerData;
		private Dictionary<string, ExerciseSheet> exerciseCollection;
		private Dictionary<int, HeaderData> headerFileDatas;
		private int[] exerciseOrder;
		#endregion

		#region const
		private const string title = "title";
		private const string subject = "subject";
		private const string verb = "verb";
		private const string compliment = "compliment";
		private const string index = "index";
		private const string tablekey = "tablekey";
		private const string tablefile = "tablefile";
		private const string keyboardfile = "keyboardfile";

		private const string standardHeader = "std_header";
		private const string beStandardHeader = "be_std_header";
		#endregion

		public StandardTableExerciseSheet(TableType tableType)
		{
			string filename = GetHeaderFilename(tableType);
			headerData = CSVReader.Read(filename);

			if(headerData == null || headerData.Count == 0)
			{
				Logger.Error("StandardTableExerciseSheet", "Constructor", "No data found for "+filename);
				return;
			}

			exerciseCollection = new Dictionary<string, ExerciseSheet>();
			headerFileDatas = new Dictionary<int, HeaderData>();

			for(int i = 0; i < headerData.Count; i++)
			{
				bool isTitle = (int)headerData[i][title] == 1 ? true : false;
				if(isTitle) continue;

				string s = headerData[i][subject].ToString();
				string v = headerData[i][verb].ToString();
				string c = headerData[i][compliment].ToString();
				int idx = (int)headerData[i][index];
				string key = headerData[i][tablekey].ToString();
				string tableFile = headerData[i][tablefile].ToString();
				string keyboardFile = headerData[i][keyboardfile].ToString();

				exerciseCollection.Add(key, new ExerciseSheet(tableFile, s, v, c, TableType.Standard));
				headerFileDatas.Add(idx, new HeaderData(idx, key, tableFile, keyboardFile));
			}

			SetExerciceOrderFromTableType(tableType);
			//exerciseOrder = new int[]{11,21,31,41, 12, 22, 32, 42, 13, 23, 33, 43, 14, 24, 34, 44};
		}

		private string GetHeaderFilename(TableType tableType)
		{
			switch(tableType)
			{
				case TableType.Standard:
					return standardHeader;
				case TableType.ToBe:
					return beStandardHeader;
				default:
				return "";
			}
		}

		private void SetExerciceOrderFromTableType(TableType tableType)
		{
			switch(tableType)
			{
				case TableType.Standard:
				exerciseOrder = new int[]{11,21,31,41, 12, 22, 32, 42, 13, 23, 33, 43, 14, 24, 34, 44};
					break;
				case TableType.ToBe:
				exerciseOrder = new int[]{11,21,31,41, 13, 23, 33, 43};
					break;
				default:
					break;
			}
		}

		public string GetTableKey(int index)
		{
			if(headerFileDatas.ContainsKey(index))
			{
				return headerFileDatas[index].TableKey;
			}

			return null;
		}

		public ExerciseSheet GetSheetAtIndex(string index)
		{
			if(exerciseCollection.ContainsKey(index))
				return exerciseCollection[index];

			return null;
		}

		public ExerciseSheet GetSheetAtIndex(int index)
		{
			if(headerFileDatas.ContainsKey(index))
			{
				string key = headerFileDatas[index].TableKey;
				return GetSheetAtIndex(key);
			}

			return null;
		}

		public string GetKeyboardFileAtIndex(int index)
		{
			if(headerFileDatas.ContainsKey(index))
			{
				return headerFileDatas[index].KeyboardFile;
			}

			return null;
		}

		public List<string[]> GetSheetHeader()
		{
			// TODO : take the title for list header into consideration, using csv list data directly instead.

			List<string[]> list = new List<string[]>();

			foreach(KeyValuePair<string, ExerciseSheet>  kvp in exerciseCollection)
			{
				string[] header = new string[]{kvp.Value.Subject, kvp.Value.Verb, kvp.Value.Compliment};
				list.Add(header);
			}

			return list;
		}

		public TableCellExerciseData GetExerciceData(string selectedVerb, int cellIndex)
		{
			if(exerciseCollection.ContainsKey(selectedVerb))
			{
				return exerciseCollection[selectedVerb].GetExerciceAtIndex(cellIndex);
			}

			return null;
		}

		public int GetCellIndexFromOrderIndex(int index)
		{
			if(index >= exerciseOrder.Length)
			{
				Logger.Error("StandardTableExerciseSheet", "GetCellIndexFromOrderIndex", "Index query is not valid : " + index);
				return -1;	
			}

			return exerciseOrder[index];
		}

		public int GetMaxTableIndex()
		{
			return exerciseCollection.Count - 1;
		}

		public int GetLastOrderIndex()
		{
			return exerciseOrder.Length - 1;
		}
	}
}

