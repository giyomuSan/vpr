﻿using System.Collections;
using System.Collections.Generic;


namespace VPR.Core
{
	public abstract class Keyboard
	{
		#region fields
		protected string filename;
		protected Dictionary<int, KeyData> keyCollection;
		#endregion

		#region methods
		protected Keyboard(string filename)
		{
			this.filename = filename;
			CreateKeyboard();
		}

		protected abstract void CreateKeyboard();

		public Dictionary<int, KeyData> GetKeyboard()
		{
			return keyCollection;
		}
		#endregion
	}
}
