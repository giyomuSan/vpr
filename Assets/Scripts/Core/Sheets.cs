﻿using UnityEngine;
using System.Collections.Generic;
using VPR.model;

namespace VPR
{
	public sealed class Sheets 
	{
		#region constant
		private const string time = "time";
		private const string mode = "mode";
		private const string parameters = "parameters";

		private readonly Dictionary<string, VerbTime> StringToVerbTimeEnum = new Dictionary<string, VerbTime>()
		{
			{"present", VerbTime.Present},
			{"past", VerbTime.Past},
			{"futur", VerbTime.Future},
			{"modal", VerbTime.Modal},
		};

		private readonly Dictionary<string, VerbMode> StringToVerbModeEnum = new Dictionary<string, VerbMode>()
		{
			{"simple", VerbMode.Simple},
			{"progressive", VerbMode.Progressive},
			{"perfect", VerbMode.Perfect},
			{"perfectprogressive", VerbMode.PerfectProgressive},
		};
		#endregion

		#region methods
		public Sheets(){}

		public List<TableCellExerciseData> GetData(string filename)
		{
			List<Dictionary<string,object>> data = CSVReader.Read(filename);

			if(data == null)
			{
				Logger.Message("Sheets", "GetData", "CSVReader returned null for filename : " + filename+" , skipping");
				return null;
			}

			var list = new List<TableCellExerciseData>();

			for(int i = 0; i < data.Count; i++)
			{
				string verbTime = data[i][time].ToString();
				string verbMode = data[i][mode].ToString();
				int index = GetIndex(StringToVerbTimeEnum[verbTime], StringToVerbModeEnum[verbMode]);

				if(data[i][parameters] != string.Empty)
				{
					object[] sentences = SplitParameters(data[i][parameters]);
					var tableData = new TableCellExerciseData(index, sentences);

					list.Add(tableData);
				}
				else
				{
					Logger.Message("Sheet", "GetData", "Null paramaters, skiping data : " + i.ToString());
				}
			}

			return list;
		}

		private int GetIndex(VerbTime verbTime, VerbMode verbMode)
		{
			int lhs = (int)verbTime;
			int rhs = (int)verbMode;

			return int.Parse(lhs.ToString() + rhs.ToString());
		}

		private object[] SplitParameters(object parameter)
		{
			//Debug.Log(parameter.ToString());

			string[] split = parameter.ToString().Split(',');
			object[] sheetData = new object[split.Length];

			int count = 0;

			foreach(var s in split)
			{
				int index = -1;
				if(int.TryParse(s, out index))
				{
					sheetData[count] = index;
				}
				else
				{
					sheetData[count] = s;
				}

				count++;
			}

			return sheetData;
		}
		#endregion
	}
}

