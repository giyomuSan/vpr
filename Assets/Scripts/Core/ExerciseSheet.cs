﻿using UnityEngine;
using System.Collections.Generic;

namespace VPR.Core
{
	public class ExerciseSheet
	{
		private string subject;
		private string verb;
		private string compliment;
		private List<TableCellExerciseData> exerciseList;

		public string Subject {get{return subject;}}
		public string Verb {get{return verb;}}
		public string Compliment {get{return compliment;}}

		public ExerciseSheet(string filename, string subject, string verb, string compliment, TableType tableType)
		{
			this.subject = subject;
			this.verb = verb;
			this.compliment = compliment;

			Sheets test = new Sheets();
			exerciseList = test.GetData(filename);

			//Logger.Message("ExerciseSheet", "Constructor", string.Format("Sheet created for {0}, {1}, {2}", this.subject, this.verb, this.compliment));
		}

		public TableCellExerciseData GetExerciceAtIndex(int index)
		{
			foreach(var exercice in exerciseList)
			{
				if(exercice.Index == index) return exercice;
			}

			return null;
		}
	}
}

