﻿using UnityEngine;
using System.Collections.Generic;

namespace VPR.Core
{
	public class DefaultTablePopup : TablePopup
	{
		public DefaultTablePopup() : base(){}

		protected override void CreateTablePopup()
		{
			// TODO : compliment not taken into consideration here , maybe need one more element for popup data, so 8 total
			tableCollection = new Dictionary<int, TablePopupData>()
			{
				{GetIndex(VerbTime.Present,VerbMode.Simple), new TablePopupData("S", "V", "", "", "", "", "1 or 2", "AV", "S", "MV", "", "", "", "1", "S", "AV", "not", "MV", "", "", "1")},
				{GetIndex(VerbTime.Present,VerbMode.Progressive), new TablePopupData("S", "AV", "MV", "", "", "", "5", "AV", "S", "MV", "", "", "", "5", "S", "AV", "not", "MV", "", "", "5")},
				{GetIndex(VerbTime.Present,VerbMode.Perfect), new TablePopupData("S", "AV", "MV", "", "", "", "4", "AV", "S", "MV", "", "", "", "4", "S", "AV", "not", "MV", "", "", "4")},
				{GetIndex(VerbTime.Present,VerbMode.PerfectProgressive), new TablePopupData("S", "AV", "AV", "MV", "", "", "5", "AV", "S", "AV", "MV", "", "", "5", "S", "AV", "not", "AV", "MV", "", "5")},

				{GetIndex(VerbTime.Past,VerbMode.Simple), new TablePopupData("S", "V", "", "", "", "", "3", "AV", "S", "MV", "", "", "", "1", "S", "AV", "not", "MV", "", "", "1")},
				{GetIndex(VerbTime.Past,VerbMode.Progressive), new TablePopupData("S", "AV", "MV", "", "", "", "5", "AV", "S", "MV", "", "", "", "5", "S", "AV", "not", "MV", "", "", "5")},
				{GetIndex(VerbTime.Past,VerbMode.Perfect), new TablePopupData("S", "AV", "MV", "", "", "", "4", "AV", "S", "MV", "", "", "", "4", "S", "AV", "not", "MV", "", "", "4")},
				{GetIndex(VerbTime.Past,VerbMode.PerfectProgressive), new TablePopupData("S", "AV", "AV", "MV", "", "", "5", "AV", "S", "AV", "MV", "", "", "5", "S", "AV", "not", "AV", "MV", "", "5")},

				{GetIndex(VerbTime.Future,VerbMode.Simple), new TablePopupData("S", "AV", "MV", "", "", "", "1", "AV", "S", "MV", "", "", "", "1", "S", "AV", "not", "MV", "", "", "1")},
				{GetIndex(VerbTime.Future,VerbMode.Progressive), new TablePopupData("S", "AV", "AV", "MV", "", "", "5", "AV", "S", "AV", "MV", "", "", "5", "S", "AV", "not", "AV", "MV", "", "5")},
				{GetIndex(VerbTime.Future,VerbMode.Perfect), new TablePopupData("S", "AV", "AV", "MV", "", "", "4", "AV", "S", "AV", "MV", "", "", "4", "S", "AV", "not", "AV", "MV", "", "4")},
				{GetIndex(VerbTime.Future,VerbMode.PerfectProgressive), new TablePopupData("S", "AV", "AV", "AV", "MV", "", "5", "AV", "S", "AV", "AV", "MV", "", "5", "S", "AV", "not", "AV", "AV", "MV", "5")},

				{GetIndex(VerbTime.Modal,VerbMode.Simple), new TablePopupData("S", "AV", "MV", "", "", "", "1", "AV", "S", "MV", "", "", "", "1", "S", "AV", "not", "MV", "", "", "1")},
				{GetIndex(VerbTime.Modal,VerbMode.Progressive), new TablePopupData("S", "AV", "AV", "MV", "", "", "5", "AV", "S", "AV", "MV", "", "", "5", "S", "AV", "not", "AV", "MV", "", "5")},
				{GetIndex(VerbTime.Modal,VerbMode.Perfect), new TablePopupData("S", "AV", "AV", "MV", "", "", "4", "AV", "S", "AV", "MV", "", "", "4", "S", "AV", "not", "AV", "MV", "", "4")},
				{GetIndex(VerbTime.Modal,VerbMode.PerfectProgressive), new TablePopupData("S", "AV", "AV", "AV", "MV", "", "5", "AV", "S", "AV", "AV", "MV", "", "5", "S", "AV", "not", "AV", "AV", "MV", "5")},
			};
		}

		private int GetIndex(VerbTime verbTime, VerbMode verbMode)
		{
			int lhs = (int)verbTime;
			int rhs = (int)verbMode;

			return int.Parse(lhs.ToString() + rhs.ToString());
		}
	}
}

