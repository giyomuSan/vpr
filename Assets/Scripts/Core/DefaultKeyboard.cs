﻿using UnityEngine;
using System.Collections.Generic;
using VPR.model;

namespace VPR.Core
{
	public class DefaultKeyboard : Keyboard
	{
		#region constant
		private const string nullKey = "null";
		private const string key = "key";
		private const string primary = "primary";
		private const string secondary = "secondary";
		private const string word = "word";
		private const string index = "index";
		private const string color = "color";
		private const string sfxname = "sfxname";

		private readonly Dictionary<string, KeyFunction> StringToKeyFunctionEnum = new Dictionary<string, KeyFunction>()
		{
			{"sub", KeyFunction.Subject},
			{"verb", KeyFunction.Verb},
			{"neg", KeyFunction.Negation},
			{"comp", KeyFunction.Complement},
			{"av", KeyFunction.AuxilliaryVerb},
			{"mv", KeyFunction.MainVerb},
			{"none", KeyFunction.None}
		};
		#endregion

		public DefaultKeyboard(string filename) : base(filename)
		{
			Logger.Message("DefaultKeybard", "Constructor", "Creating default keyboard from file : " + filename);
		}
			
		protected override void CreateKeyboard()
		{
			List<Dictionary<string,object>> keyboard = CSVReader.Read(filename);
			keyCollection = new Dictionary<int, KeyData>();

			for(int i = 0; i < keyboard.Count; i++)
			{
				bool isNull = (int)keyboard[i][nullKey] == 1 ? true : false;
				int keyIndex = (int)keyboard[i][key];

				if(isNull)
				{
					keyCollection.Add(keyIndex, null);
					continue;
				}

				KeyFunction p = StringToKeyFunctionEnum[keyboard[i][primary].ToString()];
				KeyFunction s = StringToKeyFunctionEnum[keyboard[i][secondary].ToString()];
				string w = keyboard[i][word].ToString();
				string idx = keyboard[i][index].ToString();
				Color32 col = GetColor(keyboard[i][color]);
				string soundFile = keyboard[i][sfxname].ToString();

				var keyData = new KeyData(p,s,w,idx,col, soundFile);
				keyCollection.Add(keyIndex, keyData);
			}
		}

		private Color32 GetColor(object colorValue)
		{
			string[] split = colorValue.ToString().Split(':');

			if(split.Length == 4)
			{
				int r = int.Parse(split[0]);	
				int g = int.Parse(split[1]);	
				int b = int.Parse(split[2]);	
				int a = int.Parse(split[3]);	

				return new Color32((byte)r,(byte)g,(byte)b,(byte)a);
			}

			return new Color32(255,255,255,255);
		}
	}
}

