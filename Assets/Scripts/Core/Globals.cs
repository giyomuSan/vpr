﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

#region enums
public enum KeyFunction
{
	[Description("")]
	None,
	[Description("AV")]
	AuxilliaryVerb,
	[Description("S")]
	Subject,
	[Description("Neg")]
	Negation,
	[Description("Comp")]
	Complement,
	[Description("V")]
	Verb,
	[Description("MV")]
	MainVerb
}

public enum VerbTime
{
	None,
	Present,
	Past,
	Future,
	Modal
}

public enum VerbMode
{
	None,
	[Description("Simple")]
	Simple,
	[Description("Progressive")]
	Progressive,
	[Description("Perfect")]
	Perfect,
	[Description("Perfect Progressive")]
	PerfectProgressive
}

public enum TableType
{
	[Description("std_")]
	Standard,
	[Description("be_")]
	ToBe,
	[Description("psv_")]
	Passive
}

public enum SentenceType
{
	[Description("S")]
	Affirmative,
	[Description("Q")]
	Interrogative,
	[Description("N")]
	Negative, 
}
#endregion

#region class
[System.Serializable]
public class KeyData
{
	public KeyFunction Primary;
	public KeyFunction Secondary;
	public string Word;
	public string SuffixIndex;
	public Color32 CellTint;
	public string SoundFile;
	public int CellUnit;

	public KeyData(KeyFunction primary, KeyFunction secondary, string word, string suffixIndex, Color32 cellTint, string soundFile, int cellUnit = 1)
	{
		Primary = primary;
		Secondary = secondary;
		Word = word;
		SuffixIndex = suffixIndex;
		CellTint = cellTint;
		SoundFile = soundFile;
		CellUnit = cellUnit;

	}
}

[System.Serializable]
public class FeedbackCellData
{
	public KeyFunction Function;
	public string DefaultText;

	public FeedbackCellData(KeyFunction function, string defaultText)
	{
		Function = function;
		DefaultText = defaultText;
	}
}

[System.Serializable]
public class TableData
{
	public enum CellAttribute
	{
		Blank,
		Default,
		VerbTimeHeader,
		VerbModeHeader,
	}

	public CellAttribute Attribute;
	public VerbTime Time;
	public VerbMode Mode;
	public TableLineWrapper LineWrapper;
	public Color32 Color;

	public TableData(CellAttribute attribute, VerbTime time, VerbMode mode, TableLineWrapper lineWrapper, Color32 color)
	{
		Attribute = attribute;
		Time = time;
		Mode = mode;
		LineWrapper = lineWrapper;
		Color = color;
	}
}

[System.Serializable]
public class TableLineWrapper
{
	public TableLine[] Lines;

	public TableLineWrapper(params object[] lineParameters)
	{
		if(lineParameters == null) return;
		Lines = CreateTableLines(lineParameters);
	}

	private TableLine[] CreateTableLines(params object[] lineParameters)
	{
		List<UnityEngine.Color32> colors = new List<UnityEngine.Color32>();
		List<string> numIndexes = new List<string>();

		for(int i = 0; i < lineParameters.Length; i++)
		{
			if(lineParameters[i].GetType() == typeof(UnityEngine.Color32))
			{
				colors.Add((UnityEngine.Color32)lineParameters[i]);
			}
			else if(lineParameters[i].GetType() == typeof(string))
			{
				numIndexes.Add((string)lineParameters[i]);
			}
		}

		// check all list have 3 elements
		if(colors.Count < 3)
		{
			var startIndex = 3 - (3 - colors.Count);

			for(int j = startIndex; j < 3; j++)
			{
				colors.Add(CellColor.Transparent);
			}
		}

		if(numIndexes.Count < 3)
		{
			string lastElementValue = numIndexes[numIndexes.Count - 1];
			var startIndex = 3 - (3 - numIndexes.Count);

			for(int k = startIndex; k < 3; k++)
			{
				numIndexes.Add(lastElementValue);
			}
		}
			
		TableLine[] lines = new TableLine[]
		{
			new TableLine(SentenceType.Affirmative, colors[0], numIndexes[0]),
			new TableLine(SentenceType.Interrogative, colors[1], numIndexes[1]),
			new TableLine(SentenceType.Negative, colors[2], numIndexes[2]),
		};

		return lines;
	}

}

[System.Serializable]
public class TableLine
{
	public SentenceType Sentence;
	public Color32 OverlayColor = new Color32(255,255,255,0);
	public string NumericalIndex;

	public TableLine(SentenceType sentence, Color32 overlayColor, string numericalIndex)
	{
		Sentence = sentence;
		OverlayColor = overlayColor;
		NumericalIndex = numericalIndex;
	}
}

[System.Serializable]
public class TablePopupData
{
	public List<string[]> stringArrayList;

	public TablePopupData(params string[] listParameters)
	{
		// max element written to each is 6, non existing element should be set as an empty string.
		if(listParameters.Length != 21)
		{
			Logger.Error("TablePopupData", "TablePopUpData", "Constructor : Parameters count insufficient should be 21 but only : " + listParameters.Length.ToString());
			return;
		}

		int numElement = 7;
			
		string[] subject = new string[numElement];
		string[] question = new string[numElement];
		string[] negation = new string[numElement];

		for(int i = 0; i < numElement; i++)
		{
			subject[i] = listParameters[i];
			question[i] = listParameters[i + numElement];
			negation[i] = listParameters[i + numElement * 2];
		}

		stringArrayList = new List<string[]>()
		{
			subject,
			question,
			negation
		};
	}
}

[System.Serializable]
public class TableCellExerciseData
{
	public int Index;
	public Dictionary<int, string[]> Parameters;

	public TableCellExerciseData(int index, params object[] datas)
	{
		Index = index;

		int s = 0, q = 0, n = 0;

		// Sentence index value are based on the SentenceType enum values !
		int sentenceIndex = -1;

		for(int i = 0; i < datas.Length; i++)
		{
			if(datas[i].GetType() == typeof(int)) sentenceIndex += 1;

			if(sentenceIndex == 0) s++;
			else if(sentenceIndex == 1) q++;
			else if(sentenceIndex == 2) n++;
		}
			
		s -= 1; q -= 1; n -= 1;

		string[] subject = new string[s];
		string[] question = new string[q];
		string[] negative = new string[n];

		var results = from strs in datas where strs.GetType() == typeof(string) select strs;
		var resultArray = results.ToArray();

		for(int i = 0; i < subject.Length; i++)
		{
			subject[i] = (string)resultArray[i];
		}

		for(int j = 0; j < question.Length; j++)
		{
			question[j] = (string)resultArray[j + s];
		}

		for(int k = 0; k < negative.Length; k++)
		{
			negative[k] = (string)resultArray[k + s + q];
		}

		Parameters = new Dictionary<int, string[]>()
		{
			{0, subject},
			{1, question},
			{2, negative},
		};
	}
}

public class CellColor
{
	public static Color32 Transparent = new Color32(255,255,255,0);
	public static Color32 Default = new Color32(255,255,255,255);
	public static Color32 LightRed = new Color32(243,131,155, 255);
	public static Color32 LightBrown = new Color32(252,214,177,255);
	public static Color32 DarkBrown = new Color32(215,167,118,255);
	public static Color32 Green = new Color32(149,209,149,255);
	public static Color32 LightBlue = new Color32(166,222,245,255);
	public static Color32 DarkBlue = new Color32(134,211,205,255);
	public static Color32 Purple = new Color32(208,179,210,255);
}
#endregion
