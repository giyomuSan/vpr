﻿using UnityEngine;
using System.Collections.Generic;

namespace VPR.Core
{
	public abstract class TablePopup 
	{
		#region fields
		protected Dictionary<int, TablePopupData> tableCollection;
		#endregion

		#region methods
		protected TablePopup()
		{
			CreateTablePopup();	
		}

		protected abstract void CreateTablePopup();

		public Dictionary<int, TablePopupData> GetTable()
		{
			return tableCollection;
		}
		#endregion
	}
}

