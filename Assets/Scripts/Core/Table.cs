﻿using UnityEngine;
using System.Collections.Generic;

namespace VPR.Core
{

	public abstract class Table 
	{
		#region fields
		protected string tableName;
		protected Dictionary<int, TableData> tableCollection;
		#endregion

		#region properties
		public string TableName{get{return tableName;}}
		#endregion

		#region methods
		protected Table(string tableName)
		{
			this.tableName = tableName;
			CreateTable();	
		}

		protected abstract void CreateTable();

		public Dictionary<int, TableData> GetTable()
		{
			return tableCollection;
		}
		#endregion
	}
}

