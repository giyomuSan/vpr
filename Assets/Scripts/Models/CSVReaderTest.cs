﻿using UnityEngine;
using System.Collections.Generic;

namespace VPR.model
{
	public class CSVReaderTest : MonoBehaviour
	{
		#region constant
		private const string verb = "verb";
		private const string time = "time";
		private const string mode = "mode";
		private const string sentenceIndex = "sentenceIndex";
		private const string parameters = "parameters";
		private const string compliment = "compliment";
		private const string subject = "subject";
		private const string title = "title";
		#endregion

		#region methods
		private void Awake()
		{
			// Exercice sheets reading
			List<Dictionary<string,object>> data = CSVReader.Read("Test");

			Debug.Log(data.Count);

			for(int i = 0; i < data.Count; i++) 
			{
				SplitParameters(data[i][parameters]);
			}

			// Table header reading
			/*List<Dictionary<string,object>> headerData = CSVReader.Read("std_header");

			for(int i = 0; i < headerData.Count; i++)
			{
				int t = (int)headerData[i][title];
				string s = headerData[i][subject].ToString();
				string v = headerData[i][verb].ToString();
				string c = headerData[i][compliment].ToString();

				if(t == 1)
				{
					Debug.Log("title skipped");
					continue;
				}

				string log = string.Format("S : {0}, V : {1}, C : {2}",s, v, c);
				Debug.Log(log);
			}*/
		}

		private string[] SplitParameters(object parameter)
		{
			Debug.Log(parameter.ToString());

			string[] split = parameter.ToString().Split(',');
			object[] sheetData = new object[split.Length];

			int count = 0;

			foreach(var s in split)
			{
				int index = -1;
				if(int.TryParse(s, out index))
				{
					//Debug.Log("index = " + index);	
					sheetData[count] = index;
				}
				else
				{
					//Debug.Log(s);
					sheetData[count] = s;
				}

				count++;
			}

			for(int i = 0; i < sheetData.Length; i++)
			{
				Debug.Log(sheetData[i]);
			}

			return null;
		}
		#endregion
	}
}

