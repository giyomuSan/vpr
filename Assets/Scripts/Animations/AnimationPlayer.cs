﻿using System;
using UnityEngine;

namespace VPR.Animations
{
	public class AnimationPlayer : MonoBehaviour 
	{
		#region fields
		public bool DismissOnAwake;

		[SerializeField]
		private Animator animator;
		private Action callback;
		#endregion

		#region methods
		private void Awake()
		{
			if(DismissOnAwake) Dismiss();
		}

		public void PlayAnimation(Action callback = null)
		{
			gameObject.SetActive(true);
			this.callback = callback;
		}

		public void OnAnimationEnd()
		{
			Logger.Message(this, "OnAnimationEnd", gameObject.name + " gameobject animation ended");

			if(callback != null) callback();
		}

		public void Dismiss()
		{
			gameObject.SetActive(false);	
		}
		#endregion
	}
}
