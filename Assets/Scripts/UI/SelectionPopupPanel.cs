﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using VPR.Core;
using VPR.Animations;

namespace VPR.View
{
	public class SelectionPopupPanel : PanelBase, IPointerClickHandler
	{
		#region fields
		[SerializeField]
		private Text title;
		private Dictionary<int, TablePopupData> tableCollection;
		[SerializeField]
		private PopupLineCellFrameContent[] frameContents;
		[SerializeField]
		private GameObject starImage;
		#endregion

		#region properties
		public AnimationPlayer GetAnimation{get{return starImage.GetComponent<AnimationPlayer>();}}
		#endregion

		#region methods
		protected override void Init ()
		{
			frameContents = GetComponentsInChildren<PopupLineCellFrameContent>();
			tableCollection = new DefaultTablePopup().GetTable();

			// by default the panel can not be dismissed by the user itself
			GetComponentInChildren<Button>().interactable = false;

			base.Init ();
		}
			
		public override void Show()
		{
			starImage.GetComponent<AnimationPlayer>().Dismiss();
			base.Show();
		}

		public void ShowPopUpPanel(string title, int cellIndex, Color32 baseColor, TableLine[] lines)
		{
			Logger.Message(this, "ShowPopUpPanel", "Popup with index : " + cellIndex + " required");

			this.title.text = title;

			if(tableCollection.ContainsKey(cellIndex))
			{
				for(int i = 0; i < frameContents.Length; i++)
				{
					Color32 color = lines[i].OverlayColor.a != 0 ? lines[i].OverlayColor : baseColor;
					frameContents[i].FormatCell(tableCollection[cellIndex], color);	
				}
					
				SetLineFrameVisibility(true);
				Show();

				PracticeCycleManager.Instance.StartExercise(cellIndex);

			}
		}

		public void SetLineFrameVisibility(bool isVisible)
		{
			for(int i = 0; i < frameContents.Length; i++)
			{
				frameContents[i].ResetTextElement();
				frameContents[i].ToggleTextElements(isVisible);
			}
		}

		public void HideSymbol(SentenceType sentence, int index)
		{
			for(int i = 0; i < frameContents.Length; i++)
			{
				if(frameContents[i].Sentence != sentence) continue;
				frameContents[i].ToggleTextElements(index);
			}
		}

		public void NextLine(SentenceType sentence)
		{
			for(int i = 0; i < frameContents.Length; i++)
			{
				if(frameContents[i].Sentence != sentence) continue;
				frameContents[i].SetBlinkElements();
			}
		}

		public void HideLineFrame(SentenceType sentence)
		{
			for(int i = 0; i < frameContents.Length; i++)
			{
				if(frameContents[i].Sentence != sentence) continue;
				frameContents[i].ToggleTextElements(false);
			}
		}

		public override void Dismiss ()
		{
			base.Dismiss();

			for(int i = 0; i < frameContents.Length; i++)
			{
				frameContents[i].DismissAll();
			}
		}

		public void OnPointerClick (PointerEventData eventData)
		{
			if(GetComponentInChildren<Button>().IsInteractable() == false) return;
			Dismiss();
		}
		#endregion
	}
}

