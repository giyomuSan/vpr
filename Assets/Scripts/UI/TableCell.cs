﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

namespace VPR.View
{
	public class TableCell : MonoBehaviour, IPointerClickHandler
	{
		#region fields
		[SerializeField]
		private TableCellLineFrame lineFrame;
		[SerializeField]
		private GameObject completed;
		[SerializeField]
		private TableData data;
		private int cellIndex;
		#endregion

		#region properties
		public int CellIndex{get{return cellIndex;}}
		#endregion

		#region methods
		public void SetTableCell(TableData data)
		{
			this.data = data;

			if(this.data.Attribute == TableData.CellAttribute.Blank)
			{
				GetComponent<Image>().color = data.Color;
				GetComponent<Button>().interactable = false;
				return;
			}

			// set needed data in function of the cell type.
			switch(this.data.Attribute)
			{
				case TableData.CellAttribute.VerbModeHeader:
				case TableData.CellAttribute.VerbTimeHeader:
					FormatHeaderCell();
					break;
				case TableData.CellAttribute.Default:
					FormatDefaultCell();
					SetInteractable();
					break;
			}
		}

		private void FormatHeaderCell()
		{
			GetComponent<Image>().color = data.Color;

			Text title = GetComponentInChildren<Text>();
			title.text = data.Attribute == TableData.CellAttribute.VerbModeHeader ? GetHeaderVerbModeAbbreviation(data.Mode) : GetHeaderVerbTimeAbbreviation(data.Time);

			GetComponent<Button>().interactable = false;
		}

		private string GetHeaderVerbTimeAbbreviation(VerbTime verbTime)
		{
			switch(verbTime)
			{
				case VerbTime.Present:
					return "Pr";
				case VerbTime.Past:
					return "Pa";
				case VerbTime.Future:
					return "Fu";
				case VerbTime.Modal:
					return "Mo";
				default:
					return verbTime.ToDescription();
			}
		}

		private string GetHeaderVerbModeAbbreviation(VerbMode verbMode)
		{
			switch(verbMode)
			{
				case VerbMode.Simple:
					return "Sim";
				case VerbMode.Progressive:
					return "Pro";
				case VerbMode.Perfect:
					return "Per";
				case VerbMode.PerfectProgressive:
					return "Per-Pro";
				default:
					return verbMode.ToDescription();
			}
		}

		private void FormatDefaultCell()
		{
			ToggleCompleted(false);
			//GetComponent<Image>().color = data.Color;

			//Logger.Message("Globals", "TableData", data.LineWrapper.Lines.Length.ToString());
			lineFrame.SetSentenceLine(data.LineWrapper.Lines, data.Color);

			int lhs = (int)data.Time;
			int rhs = (int)data.Mode;

			cellIndex = int.Parse(lhs.ToString() + rhs.ToString());
		}

		private void SetInteractable()
		{
			int validIndex = PracticeCycleManager.Instance.GetInteractableCellIndex;
			if(validIndex != cellIndex)
			{
				GetComponent<Button>().interactable = false;
			}
		}

		public void SetInteractable(bool interactable)
		{
			GetComponent<Button>().interactable = interactable;
		}

		public void ToggleCompleted(bool isComplete)
		{
			if(completed == null) return;
			completed.SetActive(isComplete);
		}

		public void OnPointerClick (PointerEventData eventData)
		{
			if(!GetComponent<Button>().IsInteractable()) return;

			Logger.Message(this, "OnPointerClick",  string.Format("Table with index {0} clicked", cellIndex));

			string title = data.Time.ToDescription() +" "+ data.Mode.ToDescription();
			FindObjectOfType<UIPanelManager>().SelectionPopupPanel.ShowPopUpPanel(title, cellIndex, data.Color, data.LineWrapper.Lines);
		}
		#endregion
	}
}

