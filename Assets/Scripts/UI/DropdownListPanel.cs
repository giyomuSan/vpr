﻿using UnityEngine;
using System.Collections.Generic;

namespace VPR.View
{
	public class DropdownListPanel : PanelBase
	{
		#region fields
		[SerializeField]
		private GameObject PrefabListCell;
		[SerializeField]
		private RectTransform contentFrame;
		private List<GameObject> cells;
		#endregion

		#region methods
		protected override void Init ()
		{
			cells = new List<GameObject>();

			CreateList();
			Initializer.Instance.OnTableChange += CreateList;

			base.Init ();
		}

		private void CreateList()
		{
			List<string[]> headers = Initializer.Instance.GetSheetheaderParameters();

			CheckIfCellExist();

			foreach(var header in headers)
			{
				var cell = Instantiate(PrefabListCell);
				cell.transform.SetParent(contentFrame);

				cell.GetComponent<DropdownListCell>().FormatCell(header);
				cells.Add(cell);
			}
		}

		private void CheckIfCellExist()
		{
			if(cells != null && cells.Count > 0)
			{
				foreach(var cell in cells)
				{
					Destroy(cell);
				}
			}
		}

		public override void Show ()
		{
			base.Show ();
		}

		public override void Dismiss ()
		{
			base.Dismiss ();
		}
		#endregion
	}
}

