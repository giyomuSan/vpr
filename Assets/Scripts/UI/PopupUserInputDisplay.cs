﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using VPR.Updatable;

namespace VPR.View
{
	public class PopupUserInputDisplay : MonoBehaviour
	{
		#region fields
		public SentenceType Sentence;

		private Text displayText;
		private string phrase;
		private int inputCount;
		private CursorInput cursor;
		#endregion

		#region constant
		private const int MAX_INPUT = 6;
		#endregion

		#region methods
		private void OnEnable()
		{
			if(displayText == null) displayText = GetComponent<Text>();
			if(cursor == null) cursor = transform.parent.GetComponentInChildren<CursorInput>();

			ResetInput();
			PracticeCycleManager.Instance.OnKeyBoardInput += PracticeCycleManager_KeyBoartdInput;
		}

		private void ResetInput()
		{
			phrase = string.Empty;
			displayText.text = phrase;
			inputCount = 0;
		}

		private void OnDisable()
		{
			PracticeCycleManager.Instance.OnKeyBoardInput -= PracticeCycleManager_KeyBoartdInput;
		}

		private void PracticeCycleManager_KeyBoartdInput(SentenceType currentSentence, string inputValue)
		{
			if(currentSentence != Sentence || inputCount >= MAX_INPUT) return;

			phrase += inputValue + " ";
			displayText.text = phrase;
			//cursor.UpdateCursorPosition(phrase);
			inputCount++;
		}
		#endregion
	}
}

