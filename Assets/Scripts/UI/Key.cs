﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using VPR.Updatable;

namespace VPR.View
{
	public class Key : MonoBehaviour, IPointerClickHandler
	{
		#region fields
		[SerializeField]
		private Text keyType;
		[SerializeField]
		private Text keyWord;
		[SerializeField]
		private AudioClip voice;
		[SerializeField]
		private KeyData data;
		#endregion

		#region const
		private const string femaleVoicePath = "female-voice";
		private const string maleVoicePath = "male-voice";
		#endregion

		#region methods
		public void SetKey(KeyData data)
		{
			if(data == null)
			{
				keyType.text = "";
				keyWord.text = "";

				GetComponent<Image>().enabled = false;
				GetComponent<Button>().interactable = false;
				return;
			}

			this.data = data;

			var keyTypeText = data.Primary.ToDescription();
			if(data.Secondary != KeyFunction.None)
			{
				keyTypeText += " / " + data.Secondary.ToDescription();
			}

			keyType.text = keyTypeText + data.SuffixIndex;
			keyWord.text = data.Word;

			GetComponent<Image>().color = data.CellTint;

			string soundPath = femaleVoicePath+"/"+data.SoundFile;
			voice = Resources.Load<AudioClip>(soundPath);

		}

		public void OnPointerClick (PointerEventData eventData)
		{
			if(!GetComponent<Button>().IsInteractable()) return;
			if(PracticeCycleManager.Instance.IsExerciseStarted == false) return;

			Logger.Message(this, "OnPointerClick", data.Word + " clicked");

			PracticeCycleManager.Instance.CheckInput(GetKeyValue(), voice);
		}

		public string GetKeyValue()
		{
			string suffix = !string.IsNullOrEmpty(data.SuffixIndex) ? data.SuffixIndex : null;
			return data.Word + suffix;
		}

		public void HighlightKey(bool isActive)
		{
			Logger.Message(this, "HighlightKey", "key with value : " + GetKeyValue() + " is highlighted");

			if(isActive) GetComponent<KeyHighlight>().StartHighlight();
			else GetComponent<KeyHighlight>().StopHighlight();
		}
		#endregion
	}
}

