﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace VPR.View
{
	public class DropdownListCell : MonoBehaviour
	{
		#region fields
		[SerializeField]
		private Toggle checkmark;
		[SerializeField]
		private Text subject;
		[SerializeField]
		private Text verb;
		[SerializeField]
		private Text compliment;
		#endregion

		#region methods
		public void FormatCell(string[] parameters)
		{
			if(parameters.Length != 3)
			{
				Logger.Error(this, "FormatCell", "Parameters number for string header not the value expected : " + parameters.Length);
				return;
			}

			subject.text = parameters[0];
			verb.text = parameters[1];
			compliment.text = parameters[2];

			checkmark.isOn = false;
		}
		#endregion
	}
}

