﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace VPR.View
{
	public class FeedbackCell : MonoBehaviour
	{
		#region fields
		[SerializeField]
		private Text subject;
		[SerializeField]
		private Text verb;
		[SerializeField]
		private Text compliment;
		#endregion

		#region methods
		public void SetCell(string[] currentHeader)
		{
			// for test
			subject.text = currentHeader[0];
			verb.text = currentHeader[1];
			compliment.text = currentHeader[2];
		}
		#endregion
	}
}

