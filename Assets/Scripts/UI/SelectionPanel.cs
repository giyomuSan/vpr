﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using VPR.Core;

namespace VPR.View
{
	// TODO : Rename to TablePanel instead
	public class SelectionPanel : PanelBase
	{
		#region fields
		public Text TableName;
		public GameObject PrefabCellBlank;
		public GameObject PrefabCell;
		public GameObject PrefabCellModeHeader;
		public GameObject PrefabCellTimeHeader;

		[SerializeField]
		private TableFrame tableFrame;
		private Dictionary<int, TableCell> cells;
		#endregion

		#region methods
		protected override void Init()
		{
			cells = new Dictionary<int, TableCell>();

			tableFrame = GetComponentInChildren<TableFrame>();

			// TODO :  this also be setup form file looking up the initializer data.
			var standardTable = new DefaultTable("StandardTable");
			var table = standardTable.GetTable();

			SetTableTitle();

			#if UNITY_EDITOR
			LogTable(table);
			#endif

			int tableIndex = 0;
			int rowIndex = 0;

			foreach(KeyValuePair<int, TableData> kvp in table)
			{
				if(tableIndex != 0) rowIndex = tableIndex / 5;

				var prefab = GetCellAttribute(kvp.Value.Attribute);

				var cell = Instantiate(prefab);
				cell.transform.SetParent(tableFrame.GetRowContainer(rowIndex));

				var tableCell = cell.GetComponent<TableCell>();
				tableCell.SetTableCell(kvp.Value);

				int lhs = (int)kvp.Value.Time;
				int rhs = (int)kvp.Value.Mode;
				int cellIndex = int.Parse(lhs.ToString() + rhs.ToString());

				cells.Add(cellIndex, tableCell);

				tableIndex++;
			}

			Initializer.Instance.OnTableChange += SetTableTitle;
			base.Init ();
		}

		private void SetTableTitle()
		{
			TableType type = Initializer.Instance.SelectedTableType;

			switch(type)
			{
				case TableType.Standard:
					TableName.text = "The Standard Table";
					break;
				case TableType.ToBe:
					TableName.text = "The Be Table";
					break;
				default:
					break;
			}
		}

		public GameObject GetCellAttribute(TableData.CellAttribute attribute)
		{
			switch(attribute)
			{
				case TableData.CellAttribute.Default:
					return PrefabCell;
				case TableData.CellAttribute.VerbModeHeader:
					return PrefabCellModeHeader;
				case TableData.CellAttribute.VerbTimeHeader:
					return PrefabCellTimeHeader;
				default:
					return PrefabCellBlank;
			}
		}

		public void SetNextInteractableCell(int completedIndex, int nextIndex)
		{
			cells[completedIndex].SetInteractable(false);
			cells[completedIndex].ToggleCompleted(true);
			cells[nextIndex].SetInteractable(true);
		}

		public void ResetCell(int startIndex)
		{
			foreach(KeyValuePair<int, TableCell> kvp in cells)
			{
				bool isInteractable = kvp.Key == startIndex ? true : false;
				kvp.Value.SetInteractable(isInteractable);
				kvp.Value.ToggleCompleted(false);
			}
		}
		#endregion

		#region debug
		private void LogTable(Dictionary<int, TableData> table)
		{

		}
		#endregion
	}
}

