﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using VPR.Updatable;

namespace VPR.View
{
	public class PopupLineCellFrameContent : MonoBehaviour
	{
		#region fields
		public SentenceType Sentence;

		[SerializeField]
		private Text[] textElements;
		private List<TextBlink> textBlinkList;
		private Image BackgroundColor;
		#endregion

		#region methods
		private void OnEnable()
		{
			CheckTextElement();
		}

		public void FormatCell(TablePopupData data, Color32 color)
		{
			//LogContents(data);
			CheckTextElement();

			transform.parent.GetComponent<Image>().color = color;

			string[] content = data.stringArrayList[(int)Sentence];
			textBlinkList = new List<TextBlink>();

			for(int i = 0; i < content.Length; i++)
			{
				textElements[i].text = content[i];

				var blinkText = textElements[i].gameObject.GetComponent<TextBlink>();
				if(blinkText != null)
				{
					blinkText.SetIndex(i);
					textBlinkList.Add(blinkText);
				}
			}

			SetBlinkElements();
		}

		private void CheckTextElement()
		{
			if(textElements == null || textElements.Length == 0)
			{
				textElements = GetComponentsInChildren<Text>();
			}
		}

		public void ResetTextElement()
		{
			foreach(TextBlink text in textBlinkList)
			{
				text.ResetSymbols();
			}
		}

		public void ToggleTextElements(bool show)
		{
			for(int i = 0; i < textElements.Length; i++)
			{
				textElements[i].enabled = show;
			}
		}

		public void ToggleTextElements(int index)
		{
			if(PracticeCycleManager.Instance.CurrentSentence != Sentence) return;
			foreach(TextBlink text in textBlinkList)
			{
				text.DismissSymbol(index);
			}
		}

		public void DismissAll()
		{
			if(textBlinkList != null)
			{
				foreach(TextBlink text in textBlinkList)
				{
					text.DismissSymbol();
				}
			}
		}

		public void SetBlinkElements()
		{
			if(PracticeCycleManager.Instance.CurrentSentence != Sentence) return;
			foreach(TextBlink text in textBlinkList)
			{
				text.StartBlink();
			}
		}

		#endregion

		#region debug
		private void LogContents(TablePopupData data)
		{
			string log = string.Empty;
			foreach(string[] s in data.stringArrayList)
			{
				for(int i = 0; i < s.Length; i++)
				{
					log += s[i] + ", ";
				}

				Debug.Log(log);
				log = string.Empty;
			}
		}
		#endregion
	}
}

