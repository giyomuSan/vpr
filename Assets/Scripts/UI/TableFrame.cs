﻿using UnityEngine;
using System.Collections;

namespace VPR.View
{
	public class TableFrame : MonoBehaviour
	{
		#region fields
		[SerializeField]
		private Transform[] rows;
		#endregion

		#region methods
		public Transform GetRowContainer(int index)
		{
			if(index < rows.Length) return rows[index];
			return null;
		}
		#endregion
	}
}

