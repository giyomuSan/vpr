﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace VPR.View
{
	public class FeedbackPanel : PanelBase
	{
		#region fields
		private Button dropdownBtn;
		private FeedbackCell cell;
		private DropdownListPanel dropdownPanel;
		private bool isDropdownVisible;
		#endregion

		#region constants
		private const int kCellNumber = 3;
		#endregion

		#region methods
		protected override void Init ()
		{
			cell = GetComponentInChildren<FeedbackCell>();

			dropdownPanel = FindObjectOfType<UIPanelManager>().DropdownListPanel;
			dropdownBtn = GetComponentInChildren<Button>();
			dropdownBtn.onClick.AddListener(OnDropdownButton);

			CreateContents();
			Initializer.Instance.OnSheetChange += CreateContents;
			base.Init ();
		}

		public override void Show ()
		{
			base.Show ();
		}

		public override void Dismiss()
		{
			base.Dismiss ();
		}

		private void CreateContents()
		{
			string[] currentHeader = Initializer.Instance.GetCurrentSheetHeader();
			cell.SetCell(currentHeader);
		}

		private void OnDropdownButton()
		{
			Logger.Message(this, "OnDropdownButton", "Show list");

			isDropdownVisible = !isDropdownVisible;

			if(isDropdownVisible)
			{
				dropdownPanel.Show();
			}
			else
			{
				dropdownPanel.Dismiss();
			}
		}
		#endregion
	}
}

