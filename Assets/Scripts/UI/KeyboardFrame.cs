﻿using UnityEngine;
using System.Collections;

namespace VPR.View
{
	public class KeyboardFrame : MonoBehaviour
	{
		#region fields
		[SerializeField]
		private Transform[] lines;
		#endregion

		#region methods
		public Transform GetLineContainer(int index)
		{
			if(index < lines.Length) return lines[index];

			return null;
		}
		#endregion
	}
}

