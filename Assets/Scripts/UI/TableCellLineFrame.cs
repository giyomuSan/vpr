﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace VPR.View
{
	public class TableCellLineFrame : MonoBehaviour
	{
		#region fields
		public GameObject PrefabLine;

		private TableLine[] lines;
		#endregion

		#region constant
		private const string SeparationLineTag = "SeparationLine";
		#endregion

		#region methods
		public void SetSentenceLine(TableLine[] lines, Color32 defaultColor)
		{
			if(lines == null || lines.Length == 0) return;

			this.lines = lines;

			for(int i = 0; i < lines.Length; i++)
			{
				var line = Instantiate(PrefabLine);
				line.transform.SetParent(transform);

				var color = lines[i].OverlayColor.a != 0 ? lines[i].OverlayColor : defaultColor;
				line.GetComponent<Image>().color = color;

				line.GetComponentInChildren<Text>().text = lines[i].NumericalIndex;

				if(lines[i].Sentence == SentenceType.Negative) RemoveSeparationLine(line);
			}
		}

		private void RemoveSeparationLine(GameObject line)
		{
			foreach(Transform t in line.transform)
			{
				if(t.CompareTag(SeparationLineTag))
				{
					t.gameObject.SetActive(false);
				}
			}
		}
		#endregion
	}
}

