﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace VPR.View
{
	public class TopMenuPanel : PanelBase 
	{
		#region fields
		[SerializeField]
		private Button BtnArrow;
		[SerializeField]
		private Button BtnHome;
		[SerializeField]
		private Button BtnTutorial;
		[SerializeField]
		private Button Search;
		#endregion

		#region methods
		protected override void Init ()
		{
			BtnArrow.onClick.AddListener(OnButtonArrow);
			BtnHome.onClick.AddListener(OnButtonHome);
			BtnTutorial.onClick.AddListener(OnButtonTutorial);
			Search.onClick.AddListener(OnButtonSearch);
		}

		private void OnButtonArrow()
		{
			Logger.Message(this, "OnButtonArrow", "Pressed");

			// TODO : temp for now , use it to go to previous sheet
			Initializer.Instance.LoadPreviousSheet();
		}

		private void OnButtonHome()
		{
			Logger.Message(this, "OnButtonHome", "Pressed");

			// TODO : temp for now , use it to go to next sheet
			Initializer.Instance.LoadNextSheet();
		}

		private void OnButtonTutorial()
		{
			Logger.Message(this, "OnButtonTutorial", "Pressed");

			// TODO : temp for now , use it to go loop through tables
			Initializer.Instance.LoadTable();
		}

		private void OnButtonSearch()
		{
			Logger.Message(this, "OnButtonSearch", "Pressed");
		}
		#endregion
	}
}
