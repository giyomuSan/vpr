﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using VPR.Core;

namespace VPR.View
{
	// TODO : Need to get a way to update keys with new selected verb
	public class KeyboardPanel : PanelBase
	{
		#region fields
		public GameObject PrefabKey;

		[SerializeField]
		private KeyboardFrame keyboardFrame;
		private List<Key> keys;
		#endregion

		#region methods
		protected override void Init ()
		{
			keys = new List<Key>();
			keyboardFrame = GetComponentInChildren<KeyboardFrame>();

			string filename = Initializer.Instance.GetKeyboardTableFilename();
			var keyboard = new DefaultKeyboard(filename).GetKeyboard();

			#if UNITY_EDITOR
			//LogKeyboard(keyboard);
			#endif

			int keyIndex = 0;
			int lineIndex = 0;

			foreach(KeyValuePair<int, KeyData> kvp in keyboard)
			{
				if(keyIndex != 0)lineIndex = keyIndex / 5;

				var instance = Instantiate(PrefabKey);
				instance.transform.SetParent(keyboardFrame.GetLineContainer(lineIndex));

				var key = instance.GetComponent<Key>();
				key.SetKey(kvp.Value);
				keys.Add(key);

				keyIndex++;
			}

			Initializer.Instance.OnSheetChange += CreateKeyboard;

			base.Init ();
		}

		private void CreateKeyboard()
		{
			string filename = Initializer.Instance.GetKeyboardTableFilename();
			var keyboard = new DefaultKeyboard(filename).GetKeyboard();

			int index = 0;
			foreach(KeyValuePair<int, KeyData> kvp in keyboard)
			{
				keys[index].SetKey(kvp.Value);
				index++;
			}
		}

		public void HighlightKey(string wordValue, bool isActive)
		{
			wordValue = wordValue.ToLower();

			foreach(var key in keys)
			{
				if(key.GetKeyValue() == wordValue)
				{
					key.HighlightKey(isActive);
					break;
				}
			}
		}
			
		#endregion

		#region debug
		private void LogKeyboard(Dictionary<int, KeyData> keyboard)
		{
			foreach(KeyValuePair<int, KeyData> kvp in keyboard)
			{
				string msg;
				if(kvp.Value != null)
					msg = string.Format("Index : {0}, Data : {1}, {2}, {3}, {4}", kvp.Key, kvp.Value.Primary, kvp.Value.Secondary, kvp.Value.Word, kvp.Value.CellUnit);
				else
					msg = string.Format("Index : {0} is null", kvp.Key);

				Logger.Message(this,"LogKeyboard", msg);
			}
		}
		#endregion
	}
}

